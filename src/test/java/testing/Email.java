package testing;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class Email {
	
	private WebDriver driver;
	
	@Test
	public void init() {
		
		driver.get("https://mail.google.com/mail/u/0/#inbox");
		
		///Login
		login();
		
		//Click on New Email
		sendEmail();
	}
	
	
	@BeforeTest
	public void beforeTest() {
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
	
	public void login() {
		WebElement tag1 = new WebDriverWait (driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='email']")));
		tag1.sendKeys("automationtest0621@gmail.com");
		WebElement tag2 = new WebDriverWait (driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='qhFLie']//button")));
		tag2.click();
		WebElement tag3 = new WebDriverWait (driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='password']")));
		tag3.sendKeys("Test@123456");
		WebElement tag4 = driver.findElement(By.xpath("//div[@class='qhFLie']//div[@jscontroller='Xq93uf']//button"));
		tag4.click();
	}
	
	public void sendEmail() {
		WebElement tag1 = new WebDriverWait (driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='T-I T-I-KE L3']")));
		tag1.click();
		WebElement tag2 = new WebDriverWait (driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//textarea[@class='vO' and @role='combobox']")));
		tag2.sendKeys("dcarrioncordones@gmail.com");
		WebElement tag3 = new WebDriverWait (driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='subjectbox']")));
		tag3.sendKeys("Test email sent on " + new Date().toString());
		WebElement tag4 = new WebDriverWait (driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class, 'Am Al editable')]")));
		String message = "Hello, \n\nThis is an email sent by an automated script. Have a nice rest of the day, \n\n@Dan Carrion";
		tag4.sendKeys(message);
		WebElement tag5 = new WebDriverWait (driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@role='button' and contains(@data-tooltip, 'Enter')]")));
		tag5.click();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
